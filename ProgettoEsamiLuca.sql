-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:80
-- Creato il: Gen 30, 2017 alle 15:46
-- Versione del server: 5.7.17-0ubuntu0.16.04.1
-- Versione PHP: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ProgettoEsami`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Appello`
--

CREATE TABLE `Appello` (
  `codAppello` int(11) NOT NULL,
  `Data` date NOT NULL,
  `Luogo` varchar(45) NOT NULL,
  `Ora` varchar(5) NOT NULL,
  `codEsame` int(11) NOT NULL,
  `Posti` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Appello`
--

INSERT INTO `Appello` (`codAppello`, `Data`, `Luogo`, `Ora`, `codEsame`, `Posti`) VALUES
(1, '2016-01-09', 'Via Sacchi Cesena', '09:00', 1, 100),
(2, '2016-01-11', 'Via Sacchi Cesena', '10:00', 2, 100),
(3, '2016-01-11', 'Via Sacchi Cesena', '10:00', 3, 100),
(4, '2016-01-20', 'Via Sacchi Cesena', '9:00', 1, 100),
(5, '2016-02-01', 'Via Sacchi Cesena', '10:00', 4, 99),
(6, '2016-02-08', 'Via Sacchi Cesena', '14:00', 5, 100),
(7, '2016-02-12', 'Via Sacchi Cesena', '9:00', 2, 100),
(8, '2016-02-15', 'Via Sacchi Cesena', '14:00', 6, 99),
(9, '2016-02-23', 'Via Sacchi Cesena', '10:00', 7, 99),
(10, '2016-06-11', 'Via Sacchi Cesena', '10:00', 8, 100),
(11, '2016-06-17', 'Via Sacchi Cesena', '9:00', 3, 99),
(12, '2016-06-18', 'Via Sacchi Cesena', '10:00', 5, 99),
(13, '2016-07-02', 'Via Sacchi Cesena', '14:00', 7, 99),
(14, '2016-07-11', 'Via Sacchi Cesena', '14:00', 8, 100),
(15, '2016-01-03', 'Aula Magna Cesena', '12:00', 2, 30),
(16, '2016-01-14', 'Aula Magna Cesena', '12:00', 10, 30),
(17, '2016-01-16', 'Aula Magna Cesena', '12:00', 3, 30),
(18, '2016-01-20', 'Aula Magna Cesena', '12:00', 4, 30),
(19, '2016-01-25', 'Aula Magna Cesena', '12:00', 5, 30),
(20, '2016-01-31', 'Aula Magna Cesena', '12:00', 6, 30),
(21, '2016-02-03', 'Aula Magna Cesena', '12:00', 7, 30),
(22, '2016-02-05', 'Aula Magna Cesena', '12:00', 8, 30),
(23, '2016-02-10', 'Aula Magna Cesena', '12:00', 10, 30),
(24, '2016-02-20', 'Aula Magna Cesena', '12:00', 9, 30),
(25, '2016-02-28', 'Aula Magna Cesena', '12:00', 8, 30),
(26, '2016-01-13', 'Aula Magna Cesena', '09:00', 11, 20),
(27, '2016-01-14', 'Via Sacchi Cesena', '09:00', 12, 15),
(28, '2016-01-28', 'Aula Magna Cesena', '09:00', 11, 20),
(29, '2016-01-29', 'Via Sacchi Cesena', '09:00', 12, 15),
(30, '2016-02-15', 'Aula Magna Cesena', '09:00', 11, 20),
(31, '2016-02-17', 'Via Sacchi Cesena', '09:00', 12, 15);

-- --------------------------------------------------------

--
-- Struttura della tabella `AssociazioneNotifiche`
--

CREATE TABLE `AssociazioneNotifiche` (
  `matricola` int(11) NOT NULL,
  `codNotifica` int(11) NOT NULL,
  `letto` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `AssociazioneNotifiche`
--

INSERT INTO `AssociazioneNotifiche` (`matricola`, `codNotifica`, `letto`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(1, 5, 1),
(1, 6, 1),
(1, 7, 1),
(1, 8, 1),
(1, 9, 1),
(1, 10, 1),
(1, 11, 0),
(2, 1, 0),
(2, 2, 0),
(2, 3, 0),
(2, 4, 0),
(2, 5, 0),
(2, 6, 0),
(2, 7, 0),
(2, 8, 0),
(2, 9, 0),
(2, 10, 0),
(2, 11, 0),
(3, 1, 0),
(3, 2, 0),
(3, 3, 0),
(3, 4, 0),
(3, 5, 0),
(3, 6, 0),
(3, 7, 0),
(3, 8, 0),
(3, 9, 0),
(3, 10, 0),
(3, 11, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `Esame`
--

CREATE TABLE `Esame` (
  `codEsame` int(11) NOT NULL,
  `Materia` varchar(45) NOT NULL,
  `codProfessore` varchar(16) NOT NULL,
  `codSessione` int(11) NOT NULL,
  `CFU` int(2) DEFAULT NULL,
  `Anno` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Esame`
--

INSERT INTO `Esame` (`codEsame`, `Materia`, `codProfessore`, `codSessione`, `CFU`, `Anno`) VALUES
(1, 'Tecnologie Web', '3', 15, 6, '3'),
(2, 'Basi Di Dati', '6', 15, 12, '2'),
(3, 'Analisi', '1', 15, 12, '1'),
(4, 'Elettronica', '2', 16, 6, '2'),
(5, 'Architetture degli Elaboratori', '4', 16, 12, '1'),
(6, 'Controlli Automatici', '5', 16, 6, '2'),
(7, 'Ingegneria del Software', '7', 15, 6, '3'),
(8, 'Reti', '8', 16, 6, '2'),
(9, 'Programmazione di Reti', '8', 15, 6, '3'),
(10, 'Ricerca Operativa', '9', 16, 6, '3'),
(11, 'Algebra e Geometria', '4', 15, 6, '1'),
(12, 'Programmazione', '1', 15, 12, '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'test_user', 'test@example.com', '00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc', 'f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef');

-- --------------------------------------------------------

--
-- Struttura della tabella `Notifica`
--

CREATE TABLE `Notifica` (
  `codNotifica` int(11) NOT NULL,
  `Oggetto` varchar(45) DEFAULT NULL,
  `Testo` varchar(500) DEFAULT NULL,
  `codProfessore` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Notifica`
--

INSERT INTO `Notifica` (`codNotifica`, `Oggetto`, `Testo`, `codProfessore`) VALUES
(1, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!', '6'),
(2, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!', '1'),
(3, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!', '2'),
(4, 'Posti esauriti', 'Non ci sono più posti per il mio esame', '4'),
(5, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!asbvaeuibvebviuebvivnnvwinvinvnenvjjjjjjjjjjjjjjsssssssssssssssssmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm', '3'),
(6, 'Spostamento Esame', 'L\'esame è stato spostato. L\'appello si terrà tra 10 giorni', '5'),
(7, 'Cambio Luogo', 'L\'esame si terrà in Aula Magna', '2'),
(8, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!', '8'),
(9, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!', '9'),
(10, 'Verbalizzazione', 'L\'esame è stato Verbalizzato!', '4'),
(11, 'Cambio Città', 'L\'esame si terrà a Gallipoli, invece che a Cesena', '8');

-- --------------------------------------------------------

--
-- Struttura della tabella `Partecipante`
--

CREATE TABLE `Partecipante` (
  `matricola` int(11) NOT NULL,
  `codAppello` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Partecipante`
--

INSERT INTO `Partecipante` (`matricola`, `codAppello`) VALUES
(1, 1),
(1, 3),
(1, 5),
(1, 6),
(1, 8),
(1, 9),
(1, 11),
(1, 12),
(1, 13);

-- --------------------------------------------------------

--
-- Struttura della tabella `Professore`
--

CREATE TABLE `Professore` (
  `CodiceFiscale` varchar(16) NOT NULL,
  `Nome` varchar(45) NOT NULL,
  `Cognome` varchar(45) NOT NULL,
  `DataNascita` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Professore`
--

INSERT INTO `Professore` (`CodiceFiscale`, `Nome`, `Cognome`, `DataNascita`) VALUES
('1', 'Paolo', 'Albano', '1977-08-25'),
('2', 'Rossano', 'Codeluppi', '1977-08-25'),
('3', 'Paola', 'Salomoni', '1977-08-25'),
('4', 'Catia', 'Prandi', '1977-08-25'),
('5', 'Nicola', 'Mimmo', '1977-08-25'),
('6', 'Dario', 'Maio', '1977-08-25'),
('7', 'Stefano', 'Rizzi', '1977-08-25'),
('8', 'Franco', 'Callegati', '1977-08-25'),
('9', 'Felice', 'Natale', '1977-08-25');

-- --------------------------------------------------------

--
-- Struttura della tabella `Sessione`
--

CREATE TABLE `Sessione` (
  `codSessione` int(11) NOT NULL,
  `Inizio` date NOT NULL,
  `Fine` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Sessione`
--

INSERT INTO `Sessione` (`codSessione`, `Inizio`, `Fine`) VALUES
(1, '2009-01-01', '2009-02-28'),
(2, '2009-06-01', '2009-09-28'),
(3, '2010-01-01', '2010-02-28'),
(4, '2010-06-01', '2010-09-28'),
(5, '2011-01-01', '2011-02-28'),
(6, '2011-06-01', '2011-09-28'),
(7, '2012-01-01', '2012-02-28'),
(8, '2012-06-01', '2012-09-28'),
(9, '2013-01-01', '2013-02-28'),
(10, '2013-06-01', '2013-09-28'),
(11, '2014-01-01', '2014-02-28'),
(12, '2014-06-01', '2014-09-28'),
(13, '2015-01-01', '2015-02-28'),
(14, '2015-06-01', '2015-09-28'),
(15, '2016-01-01', '2016-02-28'),
(16, '2016-06-01', '2016-09-28'),
(17, '2017-01-01', '2017-02-28'),
(18, '2017-06-01', '2017-09-28'),
(19, '2018-01-01', '2018-02-28');

-- --------------------------------------------------------

--
-- Struttura della tabella `Studente`
--

CREATE TABLE `Studente` (
  `matricola` int(11) NOT NULL,
  `Nome` varchar(45) NOT NULL,
  `Cognome` varchar(45) NOT NULL,
  `DataNascita` date NOT NULL,
  `Provincia` varchar(2) NOT NULL,
  `Via` varchar(45) NOT NULL,
  `DataImmatricolazione` date NOT NULL,
  `Email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `Foto` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Studente`
--

INSERT INTO `Studente` (`matricola`, `Nome`, `Cognome`, `DataNascita`, `Provincia`, `Via`, `DataImmatricolazione`, `Email`, `password`, `Foto`) VALUES
(1, 'Luca', 'Semprini', '1995-07-25', 'FC', 'Don G. Minzoni', '2012-07-25', 'lucsemprini@gmail.com', 'pippo', 'http://images2.corriereobjects.it/methode_image/2016/05/31/Scienze/Foto%20Gallery/AFP_BC8AH_MGTHUMB-INTERNA.jpg'),
(2, 'Pinco', 'pinchetti', '1995-07-25', 'CA', 'Giorgi', '2012-07-25', 'pinco.pinchetti@gmail.com', 'pippo2', NULL),
(3, 'Angelo', 'Moro', '1995-07-25', 'FE', 'Aldo Moro', '2012-07-25', 'angelo.moro@gmail.com', 'pippo3', NULL),
(4, 'Console', 'Tiberio', '1995-07-25', 'FI', 'Gigi', '2012-07-25', 'console.tiberio@gmail.com', 'pippo4', NULL),
(5, 'Angelica', 'Pivetti', '1995-07-25', 'FE', 'Arcangelo Gabriele', '2012-07-25', 'angelica.pivetti@gmail.com', 'pippo5', NULL),
(6, 'Francesca', 'Verni', '1995-07-25', 'FC', 'Tomba', '2012-07-25', 'vernifra@gmail.com', 'pippo6', NULL),
(7, 'Federico', 'Cosciotti', '1995-07-25', 'BO', 'Gianduia', '2012-07-25', 'federico.cosciotti@gmail.com', 'pippo7', NULL),
(8, 'Imperatore', 'Savoia', '1995-07-25', 'BO', 'Fiordi', '2012-07-25', 'imperatore.savoia@gmail.com', 'pippo8', NULL),
(9, 'Gigi', 'Riva', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'gigi.riva@gmail.com', 'pippo9', NULL),
(10, 'Antonio', 'Conte', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'antonio.conte@gmail.com', 'pippo10', NULL),
(11, 'Carlo', 'Ancelotti', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'carlo.ancelotti@gmail.com', 'pippo11', NULL),
(12, 'Max', 'Allegri', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'max.allegri@gmail.com', 'pippo12', NULL),
(13, 'Peppe', 'Poeta', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'peppe.poeta@gmail.com', 'pippo13', NULL),
(14, 'Gigi', 'Datome', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'gigi.datome@gmail.com', 'pippo14', NULL),
(15, 'Danilo', 'Gallinari', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'danilo.gallo@gmail.com', 'pippo15', NULL),
(16, 'Marco', 'Belinelli', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'marco.beli@gmail.com', 'pippo16', NULL),
(17, 'Pierluigi', 'Cotto', '1995-12-15', 'FC', 'Foggia', '2012-07-25', 'gigi.cotto@gmail.com', 'pippo17', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `Voto`
--

CREATE TABLE `Voto` (
  `matricola` int(11) NOT NULL,
  `codAppello` int(11) NOT NULL,
  `Voto` int(11) NOT NULL,
  `Verbalizzato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Voto`
--

INSERT INTO `Voto` (`matricola`, `codAppello`, `Voto`, `Verbalizzato`) VALUES
(1, 1, 23, 1),
(1, 2, 24, 1),
(1, 3, 27, 0),
(1, 5, 23, 0),
(1, 6, 26, 1),
(1, 8, 22, 1),
(1, 9, 29, 1),
(1, 10, 18, 1),
(2, 1, 24, 1),
(2, 2, 22, 0),
(2, 3, 29, 1),
(2, 5, 18, 1),
(2, 8, 21, 1),
(2, 9, 30, 1),
(2, 10, 20, 0),
(3, 1, 30, 1),
(3, 2, 27, 1),
(3, 5, 22, 0),
(3, 6, 23, 1),
(3, 8, 24, 1),
(3, 9, 25, 1),
(3, 10, 19, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `Appello`
--
ALTER TABLE `Appello`
  ADD PRIMARY KEY (`codAppello`,`codEsame`),
  ADD KEY `fk_Appello_Esame1_idx` (`codEsame`);

--
-- Indici per le tabelle `AssociazioneNotifiche`
--
ALTER TABLE `AssociazioneNotifiche`
  ADD PRIMARY KEY (`matricola`,`codNotifica`),
  ADD KEY `fk_Studente_has_Notifica_Notifica1_idx` (`codNotifica`),
  ADD KEY `fk_Studente_has_Notifica_Studente1_idx` (`matricola`);

--
-- Indici per le tabelle `Esame`
--
ALTER TABLE `Esame`
  ADD PRIMARY KEY (`codEsame`,`codProfessore`,`codSessione`),
  ADD KEY `fk_Esame_Professore1_idx` (`codProfessore`),
  ADD KEY `fk_Esame_Sessione1_idx` (`codSessione`);

--
-- Indici per le tabelle `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `Notifica`
--
ALTER TABLE `Notifica`
  ADD PRIMARY KEY (`codNotifica`),
  ADD KEY `fk_Notifica_Professore1_idx` (`codProfessore`);

--
-- Indici per le tabelle `Partecipante`
--
ALTER TABLE `Partecipante`
  ADD PRIMARY KEY (`matricola`,`codAppello`),
  ADD KEY `fk_Studente_has_Appello_Appello1_idx` (`codAppello`),
  ADD KEY `fk_Studente_has_Appello_Studente1_idx` (`matricola`);

--
-- Indici per le tabelle `Professore`
--
ALTER TABLE `Professore`
  ADD PRIMARY KEY (`CodiceFiscale`);

--
-- Indici per le tabelle `Sessione`
--
ALTER TABLE `Sessione`
  ADD PRIMARY KEY (`codSessione`);

--
-- Indici per le tabelle `Studente`
--
ALTER TABLE `Studente`
  ADD PRIMARY KEY (`matricola`),
  ADD UNIQUE KEY `Email_UNIQUE` (`Email`);

--
-- Indici per le tabelle `Voto`
--
ALTER TABLE `Voto`
  ADD PRIMARY KEY (`matricola`,`codAppello`),
  ADD KEY `fk_Studente_has_Appello_Appello2_idx` (`codAppello`),
  ADD KEY `fk_Studente_has_Appello_Studente2_idx` (`matricola`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `Appello`
--
ALTER TABLE `Appello`
  ADD CONSTRAINT `fk_Appello_Esame1` FOREIGN KEY (`codEsame`) REFERENCES `Esame` (`codEsame`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `AssociazioneNotifiche`
--
ALTER TABLE `AssociazioneNotifiche`
  ADD CONSTRAINT `fk_Studente_has_Notifica_Notifica1` FOREIGN KEY (`codNotifica`) REFERENCES `Notifica` (`codNotifica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Studente_has_Notifica_Studente1` FOREIGN KEY (`matricola`) REFERENCES `Studente` (`matricola`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `Esame`
--
ALTER TABLE `Esame`
  ADD CONSTRAINT `fk_Esame_Professore1` FOREIGN KEY (`codProfessore`) REFERENCES `Professore` (`CodiceFiscale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Esame_Sessione1` FOREIGN KEY (`codSessione`) REFERENCES `Sessione` (`codSessione`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `Notifica`
--
ALTER TABLE `Notifica`
  ADD CONSTRAINT `fk_Notifica_Professore1` FOREIGN KEY (`codProfessore`) REFERENCES `Professore` (`CodiceFiscale`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `Partecipante`
--
ALTER TABLE `Partecipante`
  ADD CONSTRAINT `fk_Studente_has_Appello_Appello1` FOREIGN KEY (`codAppello`) REFERENCES `Appello` (`codAppello`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Studente_has_Appello_Studente1` FOREIGN KEY (`matricola`) REFERENCES `Studente` (`matricola`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `Voto`
--
ALTER TABLE `Voto`
  ADD CONSTRAINT `fk_Studente_has_Appello_Appello2` FOREIGN KEY (`codAppello`) REFERENCES `Appello` (`codAppello`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Studente_has_Appello_Studente2` FOREIGN KEY (`matricola`) REFERENCES `Studente` (`matricola`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
