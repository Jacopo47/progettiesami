<?php

error_reporting(E_ALL ^ E_NOTICE);

include "login/functions.php";
include "login/db_connect.php";

sec_session_start();
if (login_check($mysqli) == false) {
    header("Location: home.php");
}

$servername = "localhost";
$username = "root";
$password = "progettoweb";
$dbname = "ProgettoEsami";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if(!$conn->set_charset("utf8")) {
  die("Error: " . $conn->connect_error);
}

function convertiData($dataEur){
  $rsl = explode ('-',$dataEur);
  $rsl = array_reverse($rsl);
  return implode($rsl,'/');
}

$matricola = $_SESSION["user_id"];

$numNotifiche=0;
$sqlNotifiche = "SELECT codNotifica FROM AssociazioneNotifiche
    WHERE matricola=$matricola AND letto=0";
$resultNotifiche = $conn->query($sqlNotifiche);
if ($resultNotifiche->num_rows > 0) {
      while($row = $resultNotifiche->fetch_assoc()) {
        $numNotifiche++;
      }
}


$appelloconfirm=-1;
if(isset($_GET['codAppello'])) {
  $GLOBALS['appelloconfirm'] = $_GET['codAppello'];
}

?>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>ProgettoEsami - Prenota</title>
  <meta name = "viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/prenotacss.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<header class="container-fluid">
  <div>
    <img class="img-responsive" src="logocompleto.jpg" alt="Logo università" >
  </div>
</header>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-left" href="home.php">
          <img src="logo_unibo.gif">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a id="home" href = "home.php">HOME</a></li>
        <li> <a id="notifiche" href="notifiche.php">NOTIFICHE <?php
              if($numNotifiche > 0) {
                ?><span class="badge"><?php echo $numNotifiche ?></span>
                <?php
              }
              ?></a></li>
        <li> <a id="prenota" class="active" href="#">PRENOTA</a> </li>
        <li> <a id="pianifica" href="pianifica.php">PIANIFICA</a></li>
        <li> <a id="panoramica" href="panoramica.php">PANORAMICA</a> </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="main col-md-8">
    <div class="container-fluid">
      <div id="container">
        <h1>PRENOTA I TUOI ESAMI  <i class="fa fa-university"></i></h1>
        <fieldset id="sceltaesame">
          <legend>Seleziona Esame</legend>
          <div class="row">
            <form method="post" action="prenota.php">
              <div class="col-md-5">
              <div class="form-group col-xs-8 col-md-8">
                <label for="sel1">Esame:</label>
                <select class="form-control" id="selexam" name="selexam">
                  <option></option>
                  <?php
                  $sql = "SELECT codEsame, Materia,
                      codProfessore, codSessione, CFU
                      FROM Esame";


                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {

                            ?><option value="<?php echo $row["codEsame"];?>"
                              <?php
                                if($_POST['selexam'] == $row["codEsame"]) {
                                  ?>selected <?php
                                }

                              $sqlVoto = "SELECT codAppello FROM Appello WHERE codEsame=". $row["codEsame"];
                              $resultVoto = $conn->query($sqlVoto);
                              if ($resultVoto->num_rows > 0) {
                                    while($rowVoto = $resultVoto->fetch_assoc()) {
                                      $sqlVoto2 ="SELECT Voto, Verbalizzato FROM Voto
                                      WHERE matricola=$matricola AND codAppello=".$rowVoto['codAppello'];
                                      $resultVoto2 = $conn->query($sqlVoto2);
                                      if ($resultVoto2->num_rows > 0) {
                                            while($rowVoto2 = $resultVoto2->fetch_assoc()) {
                                                if($rowVoto2['Verbalizzato'] == 1) {
                                                  echo "disabled";
                                                }
                                            }
                                          }
                                    }
                                  }
                                  ?>
                              ><?php echo $row["Materia"];?>
                            </option><?php
                        }
                  }
                  ?>
                </select>
                <br/><br/>
                <input type="submit" class="btn btn-danger" name="conferma"
                value="Conferma" id="confermaesame"/>
              </div>

              <div class="col-xs-4 col-md-4">
                <br/><label id="cfu" class="tofill">
                  <?php
                      $id = $_POST['selexam'];
                      $sql = "SELECT CFU FROM Esame WHERE codEsame=$id";
                      $result = $conn->query($sql);
                      if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              echo $row["CFU"];
                            }
                      }
                    ?> CFU</label>
                <br/><label id="year" class="tofill">
                  <?php
                    $id = $_POST['selexam'];
                    $sql = "SELECT Anno FROM Esame WHERE codEsame=$id";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) {
                            echo $row["Anno"] . "°";
                          }
                    }
                  ?> ANNO</label>
              </div>
            </div>

          </form>
            <div class="col-xs-10 col-md-7" id="tohide">
                <div class="table-responsive">
                  <table class="table table-hover">
                  <caption>Prove disponibili:</caption>
                   <thead>
                     <tr>
                       <th id="date">Data</th>
                       <th id="time">Ora</th>
                       <th id="prof">Docente</th>
                       <th id="prenota"></th>
                     </tr>
                   </thead>
                   <form method="get" action="conferma.php">
                   <tbody>
                     <?php
                     $sqlAppello = "SELECT codAppello, Data, Ora,
                         Luogo, Posti, codEsame
                         FROM Appello WHERE codEsame=". $_POST['selexam'];
                     $result = $conn->query($sqlAppello);
                     if ($result->num_rows > 0) {
                       $i=0;
                           while($row = $result->fetch_assoc()) {
                             ?><tr id="<?php echo $row['codAppello']?>">
                               <td headers="date"><?php echo convertiData($row['Data']); ?></td>
                               <td headers="time"><?php echo $row['Ora'] ?></td>
                               <td headers="prof"><?php
                                    $sqlProf = "SELECT codProfessore
                                        FROM Esame WHERE codEsame=". $_POST['selexam'];
                                   $resultProf = $conn->query($sqlProf);
                                   $professore = "x";
                                   if ($resultProf->num_rows > 0) {
                                         while($rowProf = $resultProf->fetch_assoc()) {
                                           $professore = $rowProf['codProfessore'];
                                         }
                                    }
                                    $sqlProf2 = "SELECT Nome, Cognome
                                          FROM Professore WHERE CodiceFiscale=". $professore;
                                    $resultProf2 = $conn->query($sqlProf2);
                                    if ($resultProf2->num_rows > 0) {
                                          while($rowProf2 = $resultProf2->fetch_assoc()) {
                                            echo $rowProf2['Cognome'] . " " . $rowProf2['Nome'];
                                          }
                                     }
                               ?></td>
                               <td headers="prenota">

                                 <?php
                                 $sqlPartecipa = "SELECT matricola, codAppello FROM Partecipante";
                                 $resultPart = $conn->query($sqlPartecipa);
                                 $partecipato = 0;
                                 if ($resultPart->num_rows > 0) {
                                       while($rowPart = $resultPart->fetch_assoc()) {
                                         if($rowPart['matricola'] == $matricola
                                          && $row['codAppello'] == $rowPart['codAppello']) {
                                           $GLOBALS['partecipato']++;
                                           ?>
                                           <strong style ="font-weight:bolder; color: Red; text-decoration: underline">
                                             Prenotato</strong>
                                           <?php
                                         }
                                       }
                                     }
                                if($partecipato == 0) {
                                   ?>
                                   <input type="submit" class="btn btn-primary" name="prenota"
                                 value="Prenota" id="prenota">

                                 <input type="hidden" name="<?php
                                   echo "appello".$i;
                                  ?>" value="<?php echo $row['codAppello'];?>">
                                  <?php
                                }
                                ?>
                             </tr>

                           <?php
                           $i++;
                         }?>
                        <input type="hidden" name="codEsame" value="<?php echo $_POST['selexam']?>">
                        <?php
                     } else {
                       if(isset($_POST['selexam'])) {

                       ?></tbody>
                       </form>
                     </table><br/>
                     <h4><span class="label label-warning">
                       Non ci sono prove disponibili al momento
                     </span></h4>
                     <?php
                       } else {
                         ?></tbody>

                         </form>
                       </table><br/>
                        <div class="alert alert-info">
                          <strong>Prenota ora!</strong>
                          <br/> Seleziona un esame e scegli tra gli appelli disponibili
                        </div>
                       <?php
                       }
                     }
                     ?>
                   </tbody>
                 </form>
                 </table>
              </div>
            </div>
            </div>
        </fieldset>
        <br/>
      </div>
    </div>
  </div>
    <?php
    if($appelloconfirm >= 0) { ?>
      <div class="aside col-md-4" style="background-color : transparent;
       border-color: transparent; border-style: none">
      <h1>ESITO PRENOTAZIONE</h1>
        <br/>
        <div class="datiesame row" id="aperturaiscrizioni">
          <?php

          $sqlPren= "SELECT Posti, codEsame
              FROM Appello WHERE codAppello=". $appelloconfirm;
          $resultPren = $conn->query($sqlPren);


          if ($resultPren->num_rows > 0) {
                while($row = $resultPren->fetch_assoc()) {
                  $newPosti = $row['Posti'] - 1;
                    if(($newPosti) > 0) {

                      $sqlInsert = "INSERT INTO Partecipante (matricola, codAppello)
                          VALUES ('$matricola', '$appelloconfirm')";

                      if ($conn->query($sqlInsert) === TRUE) {
                        $sqlUpdate = "UPDATE Appello SET Posti='$newPosti' WHERE codAppello='$appelloconfirm'";
                        $conn->query($sqlUpdate);
                      }


                      $sqlEsame="SELECT Materia FROM Esame WHERE codEsame=". $row['codEsame'];
                      $resultEame = $conn->query($sqlEsame);
                      ?>

                      <div class="datiesame alert alert-success">
                        <h3>
                          <strong>Appello prenotato!</strong><br/> Rimangono ancora
                            <?php echo $newPosti; ?> posti disponibili.
                        </h3>
                        <br/>
                        <h4>
                          Ti sei prenotato con successo per l'appello di <?php

                          echo $resultEame->fetch_assoc()['Materia'];
                        ?>
                        </h4>
                      </div>
                      <?php
                    } else {

                     ?>
                       <div class="datiesame alert alert-danger">
                         <h3>
                           <strong>
                             <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                             Errore!</strong><br/>
                              Purtroppo non ci sono più posti disponibili per l'appello di <?php
                              echo $resultEame->fetch_assoc()['Materia'];
                           ?>
                         </h3>
                       </div>
                      <?php
                    }
                }
              }
          ?>
        </div>
        <br/><br/>

    <?php
  } else {
    ?>
      <div class="aside col-md-4">
        <h2 style="border-bottom: normal">INFO <span class="glyphicon glyphicon-education"></span></h2>
          <div class="social row">
            <h4 style="margin-left: 3%; text-decoration: underline">Puoi trovare UniBo anche su:</h4>
          <ul class="social-network social-circle">
            <li><a href="https://www.facebook.com/unibo.it/" class="icoFacebook" title="UniBo on Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/unibomagazine?lang=it" class="icoTwitter" title="UniBo Magazine"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="https://it.linkedin.com/edu/universit%C3%A0-di-bologna-13901"
               class="icoLinkedin" title="UniBo on Linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="https://www.instagram.com/unibo/" class="icoInstagram" title="UniBo on Instagram"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    <?php
  }

    ?>

  </div>
</div>
<footer class="text-center">
  <a class="toTop" href="#" data-toggle="tooltip" title="TO TOP">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a> <br><br>
  <p>Servizio per la gestione degli esami universitari.</p>
  <p>Rasi - Riciputi - Semprini</p>
</footer>


<script>
    $(document).ready(function () {
        $('[data-toggle="tootltip"]').tooltip();
    });

    /*Funzione Jquery che permette di avere la parte Aside della stessa altezza di Main*/
    $(document).ready(function() {
        var heightmain = $("div.main").css("height");
        var heightlato = $("div.aside").css("height");
        if(heightmain >= heightlato) {
          $("div.aside").css("height", heightmain);
        } else {
          $("div.main").css("height", heightlato);
        }
    });

    $(document).ready(function() {
      $("option:disabled").each( function() {
        var name = $(this).text();
        $(this).text(name + " - Sostenuto");
      });
    });
</script>

</body>
