<?php

include "login/functions.php";
include "login/db_connect.php";

sec_session_start();

if (login_check($mysqli) == false)
{
    header("Location: home.php");
}

$username = "root";
$password = "progettoweb";

$server = "localhost";
$database = "ProgettoEsami";

$mysqli2 = new mysqli($server, $username, $password, $database);

if ($mysqli2->connect_error)
{
    die("Connection failed: " . $mysqli2->connect_error);
}

$matricola = $_SESSION["user_id"];

$result_notifiche = $mysqli2->query("SELECT codNotifica, letto
                                    FROM AssociazioneNotifiche
                                    WHERE matricola = $matricola");

$notifiche = 0;

if ($result_notifiche->num_rows > 0)
{
    while ($row = $result_notifiche->fetch_assoc())
    {
        if ($row['letto'] == 0)
        {
            $notifiche++;
        }
    }
}

$result_voti = $mysqli2->query("SELECT * FROM Voto
                                WHERE matricola = $matricola");

$votes = Array();

if ($result_voti->num_rows > 0)
{
    while ($votes[] = $result_voti->fetch_assoc());
}

for ($i = 0; $i < sizeOf($votes); $i++)
{
    $codAppello = $votes[$i]["codAppello"];
    $result_appello = $mysqli2->query("SELECT codEsame FROM Appello
                                       WHERE codAppello = $codAppello");

    if ($result_appello && $result_appello->num_rows > 0)
    {
        $codEsame = $result_appello->fetch_assoc()["codEsame"];
        $result_esame = $mysqli2->query("SELECT Materia FROM Esame
                                         WHERE codEsame = $codEsame");

        if ($result_esame->num_rows > 0)
        {
            $votes[$i]["Materia"] = $result_esame->fetch_assoc()["Materia"];
        }
    }
}

$json_encoded_votes = json_encode($votes);

echo "<script>console.log('$json_encoded_votes');</script>";

$i = 0;
$media = 0;

for (; $i < sizeOf($votes); $i++)
{
    $media += + $votes[$i]["Voto"];
}

$media = $media / ($i - 1);

?>

<html lang="it">

<head>
    <meta charset="UTF-8">
    <meta name = "viewport"
          content="width=device-width, initial-scale=1.0">

    <title>Studenti Online - Università di Bologna - Alma Mater Studiorum</title>

    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/panoramica.css">

    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript"
            src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>

<header class="container-fluid">
    <div>
        <img class="img-responsive" src="logocompleto.jpg" alt="Logo università" >
    </div>
</header>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-left" href="home.php">
                <img src="logo_unibo.gif">
            </a>

            <button type="button" class="navbar-toggle"
                    data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="home.php">HOME</a></li>
                <li><a href="notifiche.php">NOTIFICHE
                        <?php

                        if ($notifiche > 0)
                        {
                            echo "<span class='badge'>$notifiche</span>";
                        }

                        ?></a></li>
                <li><a href="prenota.php">PRENOTA</a> </li>
                <li><a href="pianifica.php">PIANIFICA</a></li>

                <li><a class="active"
                        href="panoramica.php">PANORAMICA</a></li>
            </ul>
        </div>

    </div>
</nav>

<div class="container">

<div class="main col-md-8" id="test">
    <script type="text/javascript">
        google.charts.setOnLoadCallback(drawChart);
        google.charts.load('current', {'packages':['corechart']});

        function drawChart() {
            var data = new google.visualization.DataTable();

            data.addColumn('string', '');
            data.addColumn('number', 'Voti');
            data.addColumn({type: 'string', role: 'tooltip'});

            var votes = null;
            votes = <?php echo json_encode($votes) ?>;

            for (var i = 0; i < votes.length - 1; i++) {
                var tooltip = votes[i]["Voto"] + " - " + votes[i]["Materia"];
                data.addRow(['', parseInt(votes[i]["Voto"]), tooltip]);
            }

            var options = {
                width: $('.cols_chart').width(), height: 300
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }

        window.addEventListener('resize', function() {
            drawChart();
        }, false);
    </script>

    <div id="chart_div"></div>
</div>

<div class="aside col-md-4">
    <p>Media = <?php echo $media ?></p>
</div>

</div>

<footer class="text-center">
    <a class="toTop" title="TO TOP"
       href="#" data-toggle="tooltip">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
    <br><br>
    <p>Servizio per la gestione degli esami universitari.</p>
    <p>Rasi - Riciputi - Semprini</p>
</footer>

</body>

</html>
