<?php


include "login/functions.php";
include "login/db_connect.php";

sec_session_start();

if (login_check($mysqli) == false) {
    header("Location: home.php");
}

require 'System.php';
ini_set('display_errors', '1');

    if (!isset($_SESSION['monthCounter'])) {
        $_SESSION['monthCounter'] = 0;
    }

    if (!isset($_GET['pianifica'])) {
        $_GET['pianifica'] = -1;
    }

    if (!isset($_GET['ritiro'])) {
        $_GET['ritiro'] = -1;
    }

    if (!isset($_GET['prenota'])) {
        $_GET['prenota'] = -1;
    }


    if (!isset($_SESSION['offset'])) {
        $_SESSION['offset'] = 0;
    }
    $system = new System();

    if (isset($_POST['decMonth'])) {
        $system->direction = false;
        $_SESSION['monthCounter'] = $_SESSION['monthCounter'] - 1;
        $_SESSION['change'] = true;
        unset($_POST['decMonth']);
    }

    if ($_GET['pianifica']!=-1) {
        $system->pianifica($_GET['codAppello'], $_GET['codEsame']);
        $_GET['pianifica']=-1;
    }

    if ($_GET['ritiro']!=-1) {
        $system->ritiro($_GET['codAppello'], $_GET['codEsame']);
        $_GET['ritiro']=-1;
    }

    if ($_GET['prenota']!=-1) {
        $system->prenota($_GET['codAppello'], $_GET['codEsame']);
        $_GET['prenota']=-1;
    }

    if (isset($_POST['upMonth'])) {
        $system->direction = true;
        $_SESSION['change'] = true;

        $_SESSION['monthCounter'] = $_SESSION['monthCounter'] + 1;
        unset($_POST['upMonth']);
    }




    $notifiche = $system->getNotifiche();



?>
<html lang="it" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>ProgettoEsami</title>
    <meta name = "viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="css/pianifica.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="pianifica.js" type="text/javascript"></script>
    <script src="jquery-3.1.1.js"></script>
</head>

<body>
<header class="container-fluid">
    <div id="#main-image">
        <img class="img-responsive" src="logocompleto.jpg" alt="Logo università" >
    </div>
</header>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-left" href="home.php">
                <img src="logo_unibo.gif">
            </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li> <a href="home.php">HOME</a></li>
                <li> <a href="notifiche.php">NOTIFICHE
                        <?php

                        if ($notifiche > 0)
                        {
                            echo "<span class='badge'>$notifiche</span>";
                        }

                        ?></a></li>
                <li> <a href="prenota.php">PRENOTA</a> </li>
                <li> <a id="pianifica" class="active" href="pianifica.php">PIANIFICA</a></li>
                <li> <a href="panoramica.php">PANORAMICA</a> </li>
            </ul>
        </div>
    </div>
</nav>

<div class="main col-sm-8">
    <div class="container-fluid">
    <div id="calendar" class="wrapper ">
        <div class="header">

            <div class="arrow-left">
            <form action="pianifica.php" method="post">
                <button type="submit" name="decMonth" style="padding: 0; border: none;"><img src="arrow-left.png" alt="Submit" height="20" width="20"/></button>
            </form>
            </div>
            <div class="arrow-right">
                <form action="pianifica.php" method="post">
                    <button type="submit" name="upMonth" style="padding: 0; border: none;"><img src="arrow-right.png" alt="Submit" height="20" width="20"/></button>
                </form>
            </div>
            <p><?php echo $system->getMonth(). " ". $system->getYear(); ?></p>


        </div>
        <div class="calendar-body">
            <div class="row weekdays">
                <div class="col-xs-1"><p>Lun</p></div>
                <div class="col-xs-1"><p>Mar</p></div>
                <div class="col-xs-1"><p>Mer</p></div>
                <div class="col-xs-1"><p>Gio</p></div>
                <div class="col-xs-1"><p>Ven</p></div>
                <div class="col-xs-1"><p>Sab</p></div>
                <div class="col-xs-1"><p>Dom</p></div>

            </div>



            <?php
                $system->displayCalendar();
            ?>

            <div class="line"></div>
            <div class="current-date"> <?php echo $system->getToday()?></div>


        </div>
    </div>

    </div>

</div>

<div class="aside col-sm-4 border-top-0">

    <h1 id="titolo-aside">Dati:</h1>


    <div class="row div-1">

        <div class="col-xs-8 row">
            <span> <p id="fEsame">Esame: </p><p id="esame"></p></span>
            <br>
        </div>
        <div class="col-xs-4 row">
            <span> <p id="fStatus">Status: </p> <p id="status"></p></span>
            <br>
        </div>
    </div>
    <div class="col-xs-12 row div-2">
        <div class="col-xs-3 child-1">
            <p id="fLuogo">Luogo:</p>
        </div>
        <div class="col-xs-9 child-2">
            <p id="luogo"></p>
        </div>
        <br>
    </div>
    <div class="row div-3">
        <div class="col-xs-8 row">
            <span> <p id="fOra">Ora: </p><p id="ora"></p></span>
            <br>
        </div>
        <div class="col-xs-4 row">
            <span> <p id="fPosti">Posti: </p><p id="posti"></p></span>
            <br>
        </div>
    </div>

    <form action="pianifica.php" method="get">
        <div class="form-group">
            <label for="codAppello">Codice Appello: </label>
            <input type="text" class="form-control" id="codAppello" name="codAppello" size="15" placeholder="Necessario per la prenotazione">
        </div>
        <div class="form-group">
            <label for="codEsame">Codice Esame:</label>
            <input type="text" class="form-control" id="codEsame" name="codEsame" size="15" placeholder="Necessario per la prenotazione">
        </div>
        <div class="buttons">
            <button type="submit" id="prenota" name="prenota" class="btn btn-success">Prenota</button>

            <button type="submit" id="pianifica" name="pianifica" class="btn btn-primary">Pianifica</button>
            <button type="submit" id="ritiro" name="ritiro" class="btn btn-danger">Ritiro</button>
        </div>

    </form>




</div>


<div class="row fixed-bottom space">
    <div class="social row">
        <div class="middle-social">
            <h4>Puoi trovare UniBo anche su:</h4>
            <div class="col-xs-12">
                <span class="list"> <ul class="social-network social-circle">
                    <li><a href="https://www.facebook.com/unibo.it/" class="icoFacebook" title="UniBo on Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/unibomagazine?lang=it" class="icoTwitter" title="UniBo Magazine"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://it.linkedin.com/edu/universit%C3%A0-di-bologna-13901"
                           class="icoLinkedin" title="UniBo on Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="https://www.instagram.com/unibo/" class="icoInstagram" title="UniBo on Instagram"><i class="fa fa-instagram"></i></a></li>
                </ul>
                </span>
            </div>
        </div>
    </div>
</div>


<footer class="text-center fixed-bottom">
    <a class="toTop" href="#" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a> <br><br>
    <p>Servizio per la gestione degli esami universitari.</p>
    <p>Rasi - Riciputi - Semprini</p>
</footer>

<script>



    $(document).ready(function() {
        var heightmain = $("div.main").css("height");
        var heightlato = $("div.aside").css("height");
        if(heightmain >= heightlato) {
            $("div.aside").css("height", heightmain);
        } else {
            $("div.main").css("height", heightlato);
        }
    });


</script>

</body>
</html>
