/**
 * Created by jacopo on 27/01/17.
 */


$(document).ready(function () {
    console.log("via");
    $("a").on({
       click: function () {
           var day = $(this).text();
           var i;
           for (i = 0; i < appelli.length; i++) {
               var app = appelli[i];
               if (app.data == day) {
                   $("#esame").text(app.esame);
                   $("#luogo").text(app.luogo);
                   $("#ora").text(app.ora);
                   $("#posti").text(app.posti);
                   $("#codAppello").val(app.codAppello);
                   $("#codEsame").val(app.codEsame);
                   var app1 = $(this).parent().attr("class");
                   switch (app1) {
                       case 'pianified':
                           $("#status").text("Pianificato");
                           $("#pianifica").hide();
                           $("#prenota").show();
                           $("#ritiro").show();
                           break;
                       case 'reserved':
                           $("#status").text("Prenotato");
                           $("#pianifica").hide();
                           $("#prenota").hide();
                           $("#ritiro").show();
                           break;

                       case 'exam':
                           $("#status").text("Disponibile");
                           $("#pianifica").show();
                           $("#prenota").show();
                           $("#ritiro").show();
                           break;
                       case 'oldConVoto':
                           if(app.voto==-1) {
                               $("#status").text("Voto non disponibile");
                           } else {
                               if (app.verb==0) {
                                   $("#status").text("Voto: " + app.voto);
                               } else {
                                   $("#status").text("Voto: " + app.voto + " VERBALIZZATO");
                               }
                           }
                           $("#pianifica").hide();
                           $("#prenota").hide();
                           $("#ritiro").hide();
                           break;
                   }
               }
           }
       }
    });
})



