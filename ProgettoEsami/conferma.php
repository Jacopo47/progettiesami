<?php


include "login/functions.php";
include "login/db_connect.php";

$servername = "localhost";
$username = "root";
$password = "progettoweb";
$dbname = "ProgettoEsami";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}


if(!$conn->set_charset("utf8")) {
  die("Error: " . $conn->connect_error);
}

///////////////////////////////DA AGGIORNARE CON IL VALORE DELLA MATRICOLA DEL LOGIN
header('Cache-Control: no cache'); //no cache
session_cache_limiter('private_no_expire'); // works
sec_session_start();

if (login_check($mysqli) == false) {
    header("Location: home.php");
}

$matricola = $_SESSION["user_id"];
/////////////////////////////////////////////////////

$numNotifiche=0;
$sqlNotifiche = "SELECT codNotifica FROM AssociazioneNotifiche
    WHERE matricola=$matricola AND letto=0";
$resultNotifiche = $conn->query($sqlNotifiche);
if ($resultNotifiche->num_rows > 0) {
      while($row = $resultNotifiche->fetch_assoc()) {
        $numNotifiche++;
      }
}

function convertiData($dataEur){
  $rsl = explode ('-',$dataEur);
  $rsl = array_reverse($rsl);
  return implode($rsl,'/');
}

?>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>ProgettoEsami - Prenota</title>
  <meta name = "viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="css/confermacss.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<header class="container-fluid">
  <div>
    <img class="img-responsive" src="logocompleto.jpg" alt="Logo università" >
  </div>
</header>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-left" href="home.php">
          <img src="logo_unibo.gif">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a id="home" href = "home.php">HOME</a></li>
        <li> <a id="notifiche" href="notifiche.php">NOTIFICHE <?php
              if($numNotifiche > 0) {
                ?><span class="badge"><?php echo $numNotifiche ?></span>
                <?php
              }
              ?></a></li>
        <li> <a id="prenota" class="active" href="prenota.php">PRENOTA</a> </li>
        <li> <a id="pianifica" href="pianifica.php">PIANIFICA</a></li>
        <li> <a id="panoramica" href="panoramica.php">PANORAMICA</a> </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="main col-md-8">
    <div class="container-fluid">
      <div id="container">
        <h1>PRENOTA I TUOI ESAMI</h1>
        <fieldset id="sceltaesame">
          <legend>Seleziona Esame</legend>
          <div class="row">
            <form method="post" action="prenota.php">
              <div class="col-md-5">
              <div class="form-group col-xs-8 col-md-8">
                <label for="sel1">Esame:</label>
                <select class="form-control" id="selexam" name="selexam">
                  <option></option>
                  <?php
                  $sql = "SELECT codEsame, Materia,
                      codProfessore, codSessione, CFU
                      FROM Esame";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            ?><option value="<?php echo $row["codEsame"];?>"
                              <?php
                                if($_GET['codEsame'] == $row["codEsame"]) {
                                  ?>selected<?php
                                }

                              $sqlVoto = "SELECT codAppello FROM Appello WHERE codEsame=". $row["codEsame"];
                              $resultVoto = $conn->query($sqlVoto);
                              if ($resultVoto->num_rows > 0) {
                                    while($rowVoto = $resultVoto->fetch_assoc()) {
                                      $sqlVoto2 ="SELECT Voto, Verbalizzato FROM Voto
                                      WHERE matricola=$matricola AND codAppello=".$rowVoto['codAppello'];
                                      $resultVoto2 = $conn->query($sqlVoto2);
                                      if ($resultVoto2->num_rows > 0) {
                                            while($rowVoto2 = $resultVoto2->fetch_assoc()) {
                                                if($rowVoto2['Verbalizzato'] == 1) {
                                                  echo "disabled";
                                                }
                                            }
                                          }
                                    }
                                  }
                              ?>
                              ><?php echo $row["Materia"];?>
                            </option><?php
                        }
                  }
                  ?>
                </select>
                <br/><br/>
                <input type="submit" class="btn btn-danger" name="conferma"
                value="Conferma" id="confermaesame"/>
              </div>

              <div class="col-xs-4 col-md-4">
                <br/><label id="cfu" class="tofill">
                  <?php
                      $id = $_GET['codEsame'];
                      $sql = "SELECT CFU FROM Esame WHERE codEsame=$id";
                      $result = $conn->query($sql);
                      if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              echo $row["CFU"];
                            }
                      }
                    ?> CFU</label>
                <br/><label id="year" class="tofill">
                  <?php
                    $id = $_GET['codEsame'];
                    $sql = "SELECT Anno FROM Esame WHERE codEsame=$id";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) {
                            echo $row["Anno"] . "°";
                          }
                    }
                  ?> ANNO</label>
              </div>
            </div>
          </form>
            <div class="col-xs-12 col-md-7" id="tohide">
                <div class="table-responsive table-fixed">
                  <table class="table table-hover">
                  <caption>Prove disponibili:</caption>
                   <thead>
                     <tr>
                       <th id="date">Data</th>
                       <th id="time">Ora</th>
                       <th id="prof">Docente</th>
                       <th id="prenota"></th>
                     </tr>
                   </thead>
                   <tbody>
                     <form id="prenota" method="get" action="conferma.php">
                     <?php
                     $sqlAppello = "SELECT codAppello, Data, Ora,
                         Luogo, Posti, codEsame
                         FROM Appello WHERE codEsame=". $_GET['codEsame'];
                     $result = $conn->query($sqlAppello);
                     if ($result->num_rows > 0) {
                           while($row = $result->fetch_assoc()) {
                             ?><tr id="<?php echo $row['codAppello'].".".$row['codEsame'] ?>"
                                name="<?php echo $row['codAppello'].".".$row['codEsame'] ?>">
                               <td headers="date"><?php echo convertiData($row['Data']); ?></td>
                               <td headers="time"><?php echo $row['Ora'] ?></td>
                               <td headers="prof"><?php
                                    $sqlProf = "SELECT codProfessore
                                        FROM Esame WHERE codEsame=". $_GET['codEsame'];
                                   $resultProf = $conn->query($sqlProf);
                                   $professore = "x";
                                   if ($resultProf->num_rows > 0) {
                                         while($row4 = $resultProf->fetch_assoc()) {
                                           $professore = $row4['codProfessore'];
                                         }
                                    }
                                    $sqlProf2 = "SELECT Nome, Cognome
                                          FROM Professore WHERE CodiceFiscale=". $professore;
                                    $nomeProfessore = "prof";
                                    $resultProf2 = $conn->query($sqlProf2);
                                    if ($resultProf2->num_rows > 0) {
                                          while($row3 = $resultProf2->fetch_assoc()) {
                                            $GLOBALS['nomeProfessore'] = $row3['Cognome'] . " " . $row3['Nome'];
                                            echo $nomeProfessore;
                                          }
                                     }
                                     $i=0;
                                     $codAppello = 0;
                                     foreach($_GET as $key=>$value) {
                                        if($key == "prenota") {
                                          $foundprenota=$GLOBALS['i'];
                                        }
                                        if($foundprenota ==( $i - 1 )) {
                                          $GLOBALS['codAppello'] = $value;
                                        }
                                        $GLOBALS['i']++;
                                      }
                               ?></td>
                               <td headers="prenota">

                                 <?php
                                 $sqlPartecipa = "SELECT matricola, codAppello FROM Partecipante";
                                 $resultPart = $conn->query($sqlPartecipa);
                                 $partecipato = 0;
                                 if ($resultPart->num_rows > 0) {
                                       while($rowPart = $resultPart->fetch_assoc()) {
                                         if($rowPart['matricola'] == $matricola && $rowPart['codAppello'] == $row['codAppello']) {
                                           $GLOBALS['partecipato']++;
                                           ?>
                                            <strong style ="font-weight:bolder; color: Red; text-decoration: underline">
                                              Prenotato</strong>
                                           <?php
                                         }
                                       }
                                     }
                                if($partecipato == 0) {
                                   ?>
                                 <input type="submit" disabled="disabled" class="btn btn-primary" name="prenota"
                                  value="Prenota" id="prenota">
                             </td>
                               <?php
                             }
                             ?>
                             </tr>
                           </form><?php
                           }
                     } else {
                       ?></tbody>
                     </table><br/>
                     <h4><span class="label label-warning">
                       Non ci sono prove disponibili al momento
                     </span></h4>
                     <?php
                     }
                     ?>
                   </tbody>
                 </table>
              </div>
            </div>
            </div>
        </fieldset>
        <br/>
      </div>
    </div>
  </div>
  <div class="aside col-md-4">
    <h1>DETTAGLI</h1>
    <fieldset id="riepilogoesame">
      <legend>Dati Appello</legend>

      <?php
      $sqlData = "SELECT Luogo, Posti, Data
          FROM Appello WHERE codAppello=". $codAppello;
      $resultData = $conn->query($sqlData);

      $postiRimanenti = 0;
      $professore = strtoupper($nomeProfessore);
      $luogo = "locus";
      $inizioiscrizioni = 0;
      $fineiscrizioni = 0;
      if ($resultData->num_rows > 0) {
            while($row = $resultData->fetch_assoc()) {
              $GLOBALS['postiRimanenti'] = $row['Posti'];
              $GLOBALS['luogo'] = $row['Luogo'];
              $date = $row['Data'];
              $newdate = strtotime ("-2 days", strtotime($date)) ;
              $newdate = date ("d/m/Y", $newdate);
              $GLOBALS['fineiscrizioni'] = $newdate;
            }
       }

       $codiceSessione = 0;
       $sqlSessione = "SELECT codSessione
           FROM Esame WHERE codEsame=". $_GET['codEsame'];
       $resultSessione = $conn->query($sqlSessione);
       if ($resultSessione->num_rows > 0) {
             while($rowSessione = $resultSessione->fetch_assoc()) {
               $GLOBALS['codiceSessione'] = $rowSessione['codSessione'];
             }
        }

        $sqlSessione2 = "SELECT Inizio
            FROM Sessione WHERE codSessione=". $codiceSessione;
        $resultSessione2 = $conn->query($sqlSessione2);
        if ($resultSessione2->num_rows > 0) {
              while($rowSessione2 = $resultSessione2->fetch_assoc()) {
                $GLOBALS['inizioiscrizioni'] = convertiData($rowSessione2['Inizio']);
              }
         }

       ?>
      <div class="datiesame row" id="aperturaiscrizioni">
        <label>Iscrizioni aperte da:</label><br/>
        <label class="tofill" id="inizioiscrizioni"><?php echo $inizioiscrizioni;?></label><br/>
        <label>a:</label><br/>
        <label class="tofill" id="termineiscrizioni"><?php echo $fineiscrizioni;?></label>
      </div>
      <br/><br/>
      <form method="get" action="prenota.php">
      <div class="datiesame row">
        <div class="row">
          <div class="col-md-6 col-xs-6">
            <label class="explanation">Posti rimanenti:</label>
          </div>
          <div class="col-md-6 col-xs-6">
            <label class="tofill" id="postirimanenti"><?php echo $postiRimanenti;?></label><br/><br/>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-xs-6">
            <label class="explanation">Professore:</label>
          </div>
          <div class="col-md-6 col-xs-6">
            <label class="tofill" id="professore"><?php echo $professore;?></label><br/><br/>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-xs-6">
            <label class="explanation">Luogo della Prova:</label>
          </div>
          <div class="col-md-6 col-xs-6">
            <label class="tofill" id="luogo"><?php echo $luogo;?></label><br/><br/>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <input type="submit" class="btn btn-primary" name="confermaprenotazione"
            value="Conferma Prenotazione" id="confermaprenotazione">
          </div>
        </div>
      </div>
      <input type="hidden" name="codAppello" value="<?php echo $codAppello?>">
    </form>
    </fieldset>

  </div>
</div>
<footer class="text-center">
  <a class="toTop" href="#" data-toggle="tooltip" title="TO TOP">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a> <br><br>
  <p>Servizio per la gestione degli esami universitari.</p>
  <p>Rasi - Riciputi - Semprini</p>
</footer>


<script>
    $(document).ready(function () {
        $('[data-toggle="tootltip"]').tooltip();
    });

    /*Funzione Jquery che permette di avere la parte Aside della stessa altezza di Main*/
    $(document).ready(function() {
        var heightmain = $("div.main").css("height");
        var heightlato = $("div.aside").css("height");
        if(heightmain >= heightlato) {
          $("div.aside").css("height", heightmain);
        } else {
          $("div.main").css("height", heightlato);
        }
    });

    $(document).ready(function() {
      $("label.tofill").on({
          mouseenter: function(){
            $(this).css("text-shadow", "5px 5px 3px darkgray");
          },
          mouseleave: function(){
            $(this).css("text-shadow", "0px 0px 0px");
          }
      });
    });

    $(document).ready(function() {
      $("option:disabled").each( function() {
        var name = $(this).text();
        $(this).text(name + " - Sostenuto");
      });


      $("input#confermaprenotazione").on({
        mouseenter : function() {
          $(this).css("text-decoration", "underline");
        },
        mouseleave : function() {
          $(this).css("text-decoration", "none");
        }
      });
    });

</script>

</body>
