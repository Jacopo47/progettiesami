<?php

class System {
    public $app;
    public $arrayMonth;
    public $translateDays;
    private $servername = "localhost";
    private $username = "root";
    private $pass = "progettoweb";
    private $conn;
    private $matricola;
    private $appelliJS = array();
    public  $direction;
    private $year;
    private $monthNumber;
    /**
     * System constructor.
     */
    public function __construct()
    {

        $this->app = getDate();
        $this->arrayMonth = array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
        $this->translateDays = array("Mon" => "Lun", "Tue" => "Mar", "Wed" => "Mer", "Thu" => "Gio", "Fri" => "Ven", "Sat" => "Sab", "Sun" => "Dom");
        $_SESSION['change']=true;
        $this->matricola = $_SESSION["user_id"];
        $app = getdate();
        $this->year = $app['year'];

    }

    public function reload() {

    }

    public function pianifica($codAppello, $codEsame) {
        if(($codAppello!=-1) && ($codEsame!=-1)) {
            $query = "insert into Pianificazione (matricola,codAppello,codEsame) VALUES (\"".$this->matricola."\", ".$codAppello.", ".$codEsame.")";
            $this->connDB();
            if (!$this->conn->query($query)) {
                echo "<script> window.alert(\"Errore nell'inserimento della pianificazione\");</script>";
            }
            $this->closeDB();
        } else {
            echo "<script> window.alert(\"Inserire Codice Appello e Codice Esame\");</script>";
        }
    }

    public function prenota($codAppello, $codEsame) {
        header("Location: ./prenota.php?codAppello=".$codAppello."&codEsame=".$codEsame);
        exit();
    }

    public function ritiro($codAppello, $codEsame) {
        if(($codAppello!=-1)) {
            $this->connDB();
            $queryPrenotazione = "delete from Partecipante where matricola=".$this->matricola." and codAppello=".$codAppello;
            $queryPianificazione = "delete from Pianificazione where matricola=".$this->matricola." and codAppello=".$codAppello." and codEsame=".$codEsame;
            $queryUpPianificazione ="SELECT * from Pianificazione WHERE matricola=".$this->matricola." and codAppello=".$codAppello." and codEsame=".$codEsame;
            $queryUpPrenotazione = "SELECT * from Partecipante WHERE matricola=".$this->matricola." and codAppello=".$codAppello;

            $res1 = $this->conn->query($queryUpPrenotazione);
            if($res1->num_rows>0) {
                if(!$this->conn->query($queryPrenotazione)) {
                    echo "<script> window.alert(\"Errore nella cancellazione della prenotazione\");</script>";
                } else {
                    echo "<script> window.alert(\"Prenotazione cancellata con successo\");</script>";
                }
            } else {
                $res2 = $this->conn->query($queryUpPianificazione);
                if($res2->num_rows>0) {
                    if(!$this->conn->query($queryPianificazione)) {
                        echo "<script> window.alert(\"Errore nella cancellazione della pianificazione\");</script>";
                    } else {
                        echo "<script> window.alert(\"Pianificazione cancellata con successo\");</script>";
                    }
                } else {
                    echo "<script> window.alert(\"Errore nella cancellazione\");</script>";
                }
            }

            $this->closeDB();
        } else {
            echo "<script> window.alert(\"Inserire Codice Appello\");</script>";
        }
    }


    /**
     * Connessione al DataBase.
     */
    private function connDB() {
        $this->conn = new mysqli($this->servername, $this->username, $this->pass);

        if ($this->conn->connect_error) {
            die("Connection failed: ".$this->conn->connect_error);
        }
        $this->conn->select_db("ProgettoEsami");
    }

    private function closeDB() {
        $this->conn->close();
    }

    public function getNotifiche() {
        $this->connDB();
        $result = $this->conn->query("SELECT codNotifica, letto
                           FROM AssociazioneNotifiche
                           WHERE matricola = $this->matricola");

        $notifiche = 0;

        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                if ($row['letto'] == 0)
                {
                    $notifiche++;
                }
            }
        }

        return $notifiche;
    }

    /**
     * Carica gli appelli, passato un mese e un anno.
     * @param $month
     * @param $year
     * @return mixed
     */
    private function  upAppelli($month, $year) {
        $this->connDB();
        $query = "SELECT codAppello, date_format(Data, \"%d\") as Data, Luogo, Ora, codEsame, Posti from  Appello where month(Data)=".$month." and year(Data)=".$year;
        $result = $this->conn->query($query);
        $appelli = array();
        $queryEsame = "SELECT Materia from Esame WHERE codEsame";
        $queryVoto = "SELECT * from Voto WHERE matricola= $this->matricola and codAppello";
        echo "<script>";
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $appelli[count($appelli)] = new Appello($row["codAppello"], $row["Data"], $row["Luogo"], $row["Ora"], $row["codEsame"], $row["Posti"]);
                $resultEs =$this->conn->query($queryEsame ." = ". $row["codEsame"]);
                $rowEs = $resultEs->fetch_assoc();
                $resVoto = $this->conn->query($queryVoto."= ".$row["codAppello"]);

                if ($resVoto->num_rows > 0) {
                    $rowVoto = $resVoto->fetch_assoc();
                    $this->appelliJS[count($this->appelliJS)] = new AppelloJavascript($row["codAppello"], $row["Data"], $row["Luogo"], $row["Ora"], $rowEs["Materia"], $row["Posti"], $row["codEsame"], $rowVoto["Voto"], $rowVoto["Verbalizzato"]);
                    $this->appelliJS[count($this->appelliJS) - 1]->getJavascriptObject();
                } else {
                    $this->appelliJS[count($this->appelliJS)] = new AppelloJavascript($row["codAppello"], $row["Data"], $row["Luogo"], $row["Ora"], $rowEs["Materia"], $row["Posti"], $row["codEsame"], -1, -1);
                    $this->appelliJS[count($this->appelliJS) - 1]->getJavascriptObject();
                }
                echo "\n";
            }
        }

        echo "var appelli = [";
        $k = count($this->appelliJS);
        $i = 0;
        foreach ($this->appelliJS as $appJS) {
            $i++;

            echo $appJS->nome;
            if ($i < $k) {
                echo ", ";
            }
        }
        echo "]; </script> ";
        $this->closeDB();
        return $appelli;
    }

    /**
     * Carica gli appelli pianificati dati un array di appelli.
     * @param $appelli
     * @return array
     */
    private function upPianificati($appelli) {
        $this->connDB();

        $date = array();
        $i = 0;
        $query = "SELECT * from Pianificazione WHERE matricola=".$this->matricola." and codAppello";


        foreach ($appelli as $a) {

            $result = $this->conn->query($query. "=". $a->codAppello);
            if ($result->num_rows > 0) {
                $date[$i] = $a->data;
                $i++;
            }
        }
        return $date;
        $this->closeDB();
    }

    private function upPrenotati($appelli) {
        $this->connDB();

        $date = array();
        $i = 0;
        $query = "SELECT * from Partecipante WHERE matricola=".$this->matricola." and codAppello";


        foreach ($appelli as $a) {
            $result = $this->conn->query($query. "=". $a->codAppello);
            if ($result->num_rows > 0) {
                $date[$i] = $a->data;
                $i++;
            }
        }
        return $date;
        $this->closeDB();
    }

    private function upVoti($appelli) {
        $this->connDB();

        $date = array();
        $i = 0;
        $query = "SELECT * from Voto WHERE matricola=".$this->matricola." and codAppello";


        foreach ($appelli as $a) {
            $result = $this->conn->query($query. "=". $a->codAppello);
            if ($result->num_rows > 0) {
                $date[$i] = $a->data;
                $i++;
            }
        }
        return $date;
        $this->closeDB();
    }

    /**
     * Ritorna il mese selezionato del calendario
     * @return mixed
     */
    public function getMonth()
    {
        $monthString = date('Y-m-t', strtotime("first day of ".$_SESSION['monthCounter'] . " month"));


        $month = substr($monthString, -5, 2);

        $this->monthNumber =$month;
        $i = 0;
        foreach ($this->arrayMonth as $m) {
            $i++;
            if ($i == $month) {
                return $m;
            }
        }

    }

    public function getYear() {
        if ($_SESSION['monthCounter'] < 0 ) {
            $app = $_SESSION['monthCounter'] / 13;
            $app = intval($app);
            $app -= 1;
        } else {
            $app = $_SESSION['monthCounter'] / 12;
            $app = intval($app);
        }
        return $this->year + $app;
    }

    /**
     * Ritorna un array con i giorni del mese da visualizzare
     * @return array
     */
    private function setMonthDay()
    {
        $monthDays = array();
        $k = 0;
        $day = 1;
        $check = false;
        if ($this->getMonth()=="Febbraio") {
            if (checkdate(2,29,$this->getYear())) {
                $lastDay = 29;
            } else {
                $lastDay = 28;
            }
        } else {
            $lastDay = date('Y-m-t', strtotime($_SESSION['monthCounter'] . " month"));
            $lastDay = substr($lastDay, -2, 2);
        }


        $firstDay = date('D', mktime(0, 0, 0, date('m', strtotime($_SESSION['monthCounter']." month")), 1));


        $date = getdate();


        foreach ($this->translateDays as $x => $x_value) {
            if ($firstDay == $x) {
                $firstDay = $x_value;
            }
        }
        switch ($firstDay) {
            case "Lun":
                $diff = -1;
                break;
            case "Mar":
                $diff = 0;
                break;
            case "Mer":
                $diff = 1;
                break;
            case "Gio":
                $diff = 2;
                break;
            case "Ven":
                $diff = 3;
                break;
            case "Sab":
                $diff = 4;
                break;
            case "Dom":
                $diff = 5;
                break;
        }
        $prevLastDay = date('Y-m-t', strtotime("last day of " . $_SESSION['monthCounter'] . " month"));
        $prevLastDay = substr($prevLastDay, -2, 2);
        for ($i = 0; $i < 6; $i++) {
            foreach ($this->translateDays as $x => $x_value) {
                if (!$check) {
                    if ($firstDay != $x_value) {
                        $monthDays[$k] = $prevLastDay - $diff;
                        $diff--;
                        $k++;
                    } else {
                        $monthDays[$k] = $day;
                        $day++;
                        $k++;
                        $check = true;
                    }
                } else {
                    if ($day > $lastDay) {
                        $day = 1;
                    }
                    $monthDays[$k] = $day;
                    $day++;
                    $k++;
                }
            }
        }

        return $monthDays;
    }

    public function getToday() {
        $app = getdate();
        return $app['mday']. " ". $app['month']. " ". $app['year'];
    }

    /**
     * Crea il calendario a schermo.
     */
    public function displayCalendar()
    {
        $days = $this->setMonthDay();

        $month = $this->monthNumber;

        $year = date('Y', strtotime($_SESSION['monthCounter'] . " month"));
        $appelli = $this->upAppelli($month, $year);
        $k = 0;
        $check = false;
        $check2 = false;
        $checkOld = false;
        $pianificati = $this->upPianificati($appelli);
        $prenotati = $this->upPrenotati($appelli);
        $voti = $this->upVoti($appelli);
        for ($i = 0; $i < 6; $i++) {
            echo "<div class=\"row dates\">";
            foreach ($this->translateDays as $x => $x_value) {
                if ($days[$k] == 1) {
                    $check = true;
                }


                if (!$checkOld) {
                    if ((!$check) || ($check2)) {
                        echo "<div class=\"col-xs-1\"><p class=\"inactive\">" . $days[$k] . "</p></a></div>";
                    } else {
                        if ($_SESSION['monthCounter']<0) {
                            if($this->checkVoti($appelli, $days[$k])) {
                                echo "<div class=\"col-xs-1 \"><div class='oldConVoto'> <a href=\"#\"><p>" . $days[$k] . "</p></a></div></div>";
                            } else {
                                echo "<div class=\"col-xs-1\"><a href=\"#\"><p>" . $days[$k] . "</p></a></div>";
                            }
                            $checkOld = true;
                        } else if ($_SESSION['monthCounter']==0) {
                            $date = getdate();
                            if (($days[$k] <= $date['mday']) || ($check2)) {
                                if($this->checkVoti($appelli, $days[$k])) {
                                    echo "<div class=\"col-xs-1 \"><div class='oldConVoto'> <a href=\"#\"><p>" . $days[$k] . "</p></a></div></div>";
                                } else {
                                    echo "<div class=\"col-xs-1\"><a href=\"#\"><p>" . $days[$k] . "</p></a></div>";
                                }
                                $checkOld = true;
                            }
                        }
                        if (!$checkOld) {
                            if ($this->checkPrenotato($prenotati, $days[$k])) {
                                echo "<div class=\"col-xs-1 \"><div class='reserved'> <a href=\"#\"><p>" . $days[$k] . "</p></a></div></div>";
                            } else {
                                if ($this->checkPianificato($pianificati, $days[$k])) {
                                    echo "<div class=\"col-xs-1 \"><div class='pianified'> <a href=\"#\"><p>" . $days[$k] . "</p></a></div></div>";
                                } else {


                                    if (!$this->checkAppello($appelli, $days[$k])) {
                                        echo "<div class=\"col-xs-1\"><a href=\"#\"><p>" . $days[$k] . "</p></a></div>";
                                    } else {
                                        echo "<div class=\"col-xs-1 \"><div class='exam'> <a href=\"#\"><p>" . $days[$k] . "</p></a></div></div>";
                                    }
                                }
                            }
                        }
                    }
                }
                $checkOld = false;
                $k++;
                if ($k < count($days)) {
                    if (($days[$k] == "1") && ($check)) {
                        $check2 = true;
                    }
                }

            }
            echo "</div>";
        }
    }

    private function checkPrenotato($giorni, $giorno) {
        foreach ($giorni as $g) {
            if ($g == $giorno) {
                return true;
            }
        }
    }
    private function checkPianificato ($giorni, $giorno) {
        foreach ($giorni as $g) {
            if ($g == $giorno) {
                return true;
            }
        }
        return false;
    }
    private function checkAppello($appelli, $giorno) {
        foreach($appelli as $a) {
            if($a->data==$giorno) {
                return true;
            }
        }
        return false;
    }

    private function checkVoti($appelli, $giorno) {
        foreach($appelli as $a) {
            if($a->data==$giorno) {
                return true;
            }
        }
        return false;
    }

}

class Day {
    public $number;
    public $name;
    public $appello;

}

class Appello {
    public $codAppello;
    public $data;
    public $luogo;
    public $ora;
    public $codEsame;
    public $posti;

    public function __construct($codAppello, $data, $luogo, $ora, $codEsame, $posti) {
        $this->codAppello = $codAppello;
        $this->data = $data;
        $this->luogo = $luogo;
        $this->ora = $ora;
        $this->codEsame = $codEsame;
        $this->posti = $posti;

    }
}

class Studente {
    public $matricola;
    public $nome;
    public $cognome;
    public $dataDiNascita;
    public $provincia;
    public $via;
    public $dataImmatricolazione;
    public $email;
    public $password;
    public $foto;
}

class Partecipante {
    public $matricola;
    public $codAppello;
    public $ora;
}

class AssociazioneNotifiche {
    public $matricola;
    public $codNotifica;
    public $letto;
}

class Voto {
    public $matricola;
    public $codAppello;
    public $voto;
    public $verbalizzato;
}

class Notifica {
    public $codNotifica;
    public $oggetto;
    public $testo;
    public $codProfessore;
}

class Esame {
    public $codEsame;
    public $materia;
    public $codProfessore;
    public $codSessione;
}

class Professore {
    public $codiceFiscale;
    public $nome;
    public $cognome;
    public $dataNascita;
}

class appelloJavascript {
    public $codAppello;
    public $data;
    public $luogo;
    public $ora;
    public $esame;
    public $posti;
    public $nome;
    public $codEsame;
    public $voto;
    public $verbalizzato;

    public function __construct($codAppello, $data, $luogo, $ora, $esame, $posti, $codEsame, $voto, $verbalizzato) {
        $this->codAppello = $codAppello;
        $this->data = $data;
        $this->luogo = $luogo;
        $this->ora = $ora;
        $this->esame = $esame;
        $this->posti = $posti;
        $this->codEsame = $codEsame;
        $this->nome = "appello".$data;
        $this->verbalizzato = $verbalizzato;
        $this->voto = $voto;
    }

    public function getJavascriptObject() {
        echo "var ".$this->nome." = { codAppello: ".$this->codAppello.", data: ".$this->data.", luogo: '".$this->luogo."', 
                ora: \"".$this->ora."\", esame: '".$this->esame."', posti: ".$this->posti.", codEsame: \"".$this->codEsame."\", voto: ".$this->voto.", verb: ".$this->verbalizzato."};";
    }


}

?>
