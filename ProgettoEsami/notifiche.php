<?php


include "login/functions.php";
include "login/db_connect.php";


$servername = "localhost";
$username = "root";
$password = "progettoweb";
$dbname = "ProgettoEsami";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if(!$conn->set_charset("utf8")) {
  die("Error: " . $conn->connect_error);
}


///////////////////////////////DA AGGIORNARE CON IL VALORE DELLA MATRICOLA DEL LOGIN
sec_session_start();

if (login_check($mysqli) == false)
{
    header("Location: home.php");
}

$matricola = $_SESSION["user_id"];
if(isset($_GET['readAll'])) {

  $sqlUpdate = "UPDATE AssociazioneNotifiche
    SET letto=1 WHERE matricola=$matricola";
  $resultUpdate = $conn->query($sqlUpdate);

} else {

  if(isset($_GET['codNotificaToRead'])) {
    $sqlUpdate = "UPDATE AssociazioneNotifiche
      SET letto=1 WHERE matricola=$matricola AND codNotifica=".$_GET['codNotificaToRead'];
    $resultUpdate = $conn->query($sqlUpdate);
  }

}
$numNotifiche=0;
$sqlNotifiche = "SELECT codNotifica, letto FROM AssociazioneNotifiche
    WHERE matricola=$matricola";
$resultNotifiche = $conn->query($sqlNotifiche);

if ($resultNotifiche->num_rows > 0) {
      while($row = $resultNotifiche->fetch_assoc()) {
        if($row['letto'] == 0) {
          $numNotifiche++;
        }
      }
}



?>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>ProgettoEsami - Notifiche</title>
  <meta name = "viewport" content="width=device-width, initial-scale=1.0">
  <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/notifichecss.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>

<body>
<header class="container-fluid">
  <div>
    <img class="img-responsive" src="logocompleto.jpg" alt="Logo università" >
  </div>
</header>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-left" href="home.php">
          <img src="logo_unibo.gif">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a id="home" href = "home.php">HOME</a></li>
        <li> <a id="notifiche" class="active" href="#">NOTIFICHE <?php
              if($numNotifiche > 0) {
                ?><span class="badge"><?php echo $numNotifiche ?></span>
                <?php
              }
              ?></a></li>
        <li> <a id="prenota" href="prenota.php">PRENOTA</a> </li>
        <li> <a id="pianifica" href="pianifica.php">PIANIFICA</a></li>
        <li> <a id="panoramica" href="panoramica.php">PANORAMICA</a> </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <h1 style="margin-left : 1%">LE TUE NOTIFICHE</h1>
    <h2 style="margin-left : 2%">
    <a href="notifiche.php?readAll=1" class="btn btn-default">
      <span class="glyphicon glyphicon-th-list"></span>  Segna tutte come già lette
     </a>
    </h2>
    <div class="main col-md-8">

        <div class="panel panel-default cont-table">
        <table class="table table-hover table-responsive table-fixedheader table-scroll table-fixed">
          <thead>
            <tr>
              <th class="col-xs-2 row-1 from" id="from">Da</th>
              <th class="col-xs-2 row-2 obj" id="obj">Oggetto</th>
              <th class=" col-xs-5 row-3 text" id="textmsg">Testo</th>
              <th class=" col-xs-3 row-4 segna" id="check">Segna
                <span class="glyphicon glyphicon-check"></span>
              </th>
            </tr>
          </thead>
          <tbody style="height:70%">
            <?php
            $sql = "SELECT codNotifica, letto FROM AssociazioneNotifiche
                WHERE matricola=$matricola";

            if(isset($_GET['codNotificaToRead'])) {
              $sqlUpdate = "UPDATE AssociazioneNotifiche
                SET letto=1 WHERE matricola=$matricola AND codNotifica=".$_GET['codNotificaToRead'];
              $resultUpdate = $conn->query($sqlUpdate);
            }
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {

                    $sql2 = "SELECT Oggetto, Testo, codProfessore FROM Notifica
                        WHERE codNotifica=".$row['codNotifica'];
                    $result2 = $conn->query($sql2);

                    if ($result2->num_rows > 0) {
                          while($row2 = $result2->fetch_assoc()) {
                            $sql3 = "SELECT Nome, Cognome FROM Professore
                                WHERE CodiceFiscale=".$row2['codProfessore'];
                            $result3 = $conn->query($sql3);

                            if($result3->num_rows > 0) {
                                while($row3 = $result3->fetch_assoc()) {
                                  if($row['letto'] == 0) {
                                    ?>
                                    <tr style="font-weight: bolder; background-color: #e6e6e6">
                                  <?php
                                } else {
                                  ?>
                                  <tr>
                                    <?php
                                }
                                     ?>
                                    <td class="col-xs-2" headers="from">
                                       <?php echo $row3['Nome'];?><br/><?php echo $row3['Cognome'];?></td>
                                  <?php
                                }
                            }

                            ?>
                            <td class="col-xs-2" headers="obj"><?php echo $row2['Oggetto']; ?></td>
                            <td class="toExpand col-xs-5" headers="textmsg"><?php echo $row2['Testo']; ?></td>
                            <td style="text-align:center" class="col-xs-3" headers="check">

                              <?php
                                if($row['letto'] == 0) {
                                    ?>
                                  <a href="notifiche.php?codNotificaToRead=<?php echo $row['codNotifica'];?>"
                                     class="read btn btn-info">Leggi</a>
                                     <?php
                                } else {
                              ?>
                              <a href="notifiche.php?codNotificaToRead=<?php echo $row['codNotifica'];?>"
                                 class="read btn btn-info">Letto <span class="glyphicon glyphicon-ok"></span></a>

                               </td>
                            <?php
                            }
                          }
                    }
                    ?>
                    </tr><?php
                  }
            } else {
              ?>
              <div class="alert alert-warning">
                <span class="glyphicon glyphicon-alert"></span><strong> Non ci sono Notifiche!</strong>
                <br/>
                 Al momento non hai notifiche da visualizzare.
              </div>

              <?php
            }

            ?>
          </tbody>
        </table>
      </div>
    </div>

    <div class="aside col-md-4">
      <div class="social-cont">
      <h2 class="social" style="border-bottom: normal">INFO <span class="glyphicon glyphicon-education"></span></h2>
        <div class="social row">
          <h4 style="margin-left: 3%; text-decoration: underline">Puoi trovare UniBo anche su:</h4>
        <ul class="social-network social-circle">
          <li><a href="https://www.facebook.com/unibo.it/" class="icoFacebook" title="UniBo on Facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/unibomagazine?lang=it" class="icoTwitter" title="UniBo Magazine"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="https://it.linkedin.com/edu/universit%C3%A0-di-bologna-13901"
             class="icoLinkedin" title="UniBo on Linkedin"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="https://www.instagram.com/unibo/" class="icoInstagram" title="UniBo on Instagram"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<footer class="text-center">
    <a class="toTop" href="#" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a> <br><br>
    <p>Servizio per la gestione degli esami universitari.</p>
    <p>Rasi - Riciputi - Semprini</p>
</footer>

<script>
    $(document).ready(function () {
        $('[data-toggle="tootltip"]').tooltip();
    });

    $(document).ready(function() {

      $("a.read").on({
        mouseenter : function() {
          $(this).css("text-decoration", "underline");
        },
        mouseleave : function() {
          $(this).css("text-decoration", "none");
        }
      });

      $("a.read").click(function() {
        $(this).parent().parent().css("font-weight", "normal");
      });
    });

    /*Funzione Jquery che permette di avere la parte Aside della stessa altezza di Main*/
    $(document).ready(function() {
        var heightmain = $("div.main").css("height");
        var heightlato = $("div.aside").css("height");
        if(heightmain >= heightlato) {
          $("div.aside").css("height", heightmain);
        } else {
          $("div.main").css("height", heightlato);
        }
    });
</script>

</body>
</html>
