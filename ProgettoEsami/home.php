<?php

include "login/functions.php";
include "login/db_connect.php";

sec_session_start();

$username = "root";
$password = "progettoweb";

$server = "localhost";
$database = "ProgettoEsami";

$mysqli2 = new mysqli($server, $username, $password, $database);

if ($mysqli2->connect_error)
{
    die("Connection failed: " . $mysqli2->connect_error);
}

if (login_check($mysqli) == true)
{
    $matricola = $_SESSION['user_id'];

    $result = $mysqli2->query("SELECT codNotifica, letto
                              FROM AssociazioneNotifiche
                              WHERE matricola = $matricola");

    $notifiche = 0;

    if ($result->num_rows > 0)
    {
        while ($row = $result->fetch_assoc())
        {
            if ($row['letto'] == 0)
            {
                $notifiche++;
            }
        }
    }
}

?>

<html lang="it">

<head>
    <meta charset="UTF-8">
    <meta name = "viewport"
          content="width=device-width, initial-scale=1.0">

    <title>Studenti Online - Università di Bologna - Alma Mater Studiorum</title>

    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">

    <script type="text/javascript" src="login/forms.js"></script>
    <script type="text/javascript" src="login/sha512.js"></script>

    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript"
            src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>

<body>

<header class="container-fluid">
    <div>
        <img class="img-responsive" src="logocompleto.jpg" alt="Logo università" >
    </div>
</header>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-left" href="home.php">
                <img src="logo_unibo.gif">
            </a>

            <button type="button" class="navbar-toggle"
                    data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a class="active"
                        href="home.php">HOME</a></li>

                <li><a href="notifiche.php">NOTIFICHE
                        <?php

                        if (isset($notifiche) && $notifiche > 0)
                        {
                            echo "<span class='badge'>$notifiche</span>";
                        }

                        ?></a></li>
                <li><a href="prenota.php">PRENOTA</a> </li>
                <li><a href="pianifica.php">PIANIFICA</a></li>
                <li><a href="panoramica.php">PANORAMICA</a></li>
            </ul>
        </div>

    </div>
</nav>

<div class="container">

<div class="main col-md-8">
    <ul class="row">
        <li class="box col-xs-6 col-sm-3">
            <div class="inner-box">
                <a class="btn btn-primary btn-block"
                   href="notifiche.php">Notifiche
                    <span class="glyphicon glyphicon-bell"></span>
                </a>
                <p>Notifiche dell'Università</p>
            </div>
        </li>
        <li class="box col-xs-6 col-sm-3">
            <div class="inner-box">
                <a class="btn btn-primary btn-block"
                   href="prenota.php">Prenota
                    <span class="glyphicon glyphicon-dashboard"></span>
                </a>
                <p>Prenota un esame</p>
            </div>
        </li>
        <li class="box col-xs-6 col-sm-3">
            <div class="inner-box">
                <a class="btn btn-primary btn-block"
                   href="pianifica.php">Pianifica
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
                <p>Pianifica le date degli esami</p>
            </div>
        </li>
        <li class="box col-xs-6 col-sm-3">
            <div class="inner-box">
                <a class="btn btn-primary btn-block"
                   href="panoramica.php">Panoramica
                    <span class="glyphicon glyphicon-stats"></span>
                </a>
                <p>Panoramica degli esami svolti</p>
            </div>
        </li>
    </ul>
</div>

<div class="aside col-md-4">
    <?php if (login_check($mysqli) == false) : ?>

        <form action="login/process_login.php"
              method="post" name="login_form">

            Email: <input class="form-control"
                          type="text" name="email" id="email"/><br/>
            Password: <input class="form-control"
                             type="password" name="p" id="password"/><br/>

            <input class="btn" type="button" value="Login"
                   onclick="formhash(this.form, this.form.password);"/>
        </form>

        <?php

        if (isset($_GET['error']))
        {
            echo 'Errore di login!';
        }

        ?>

    <?php else:

        $matricola = $_SESSION['user_id'];

        $result = $mysqli2->query("SELECT * FROM Studente
                                   WHERE matricola = $matricola");

        $studente = $result->fetch_assoc();

        $foto = $studente["Foto"];
        $nome = $studente["Nome"];
        $email = $studente["Email"];
        $cognome = $studente["Cognome"];

        if ($foto == "")
        {
            $foto = "https://starc.unibo.it/images/UserPhoto.jpg";
        }

        echo "<script>console.log('$foto');</script>";

    ?>

        <div class="photo">
            <img src="<?php echo $foto ?>"
                 alt="Foto" width="64" height="64">
        </div>

        <div class="info">
            <h4><?php echo $nome, " ", $cognome ?></h4>
            <p><?php echo $email ?></p>
        </div>

        <div class="logout-button">
            <input class="btn" type="button" value="Logout"
                   onclick="location.href='login/logout.php'"/>
        </div>

    <?php endif; ?>
</div>

</div>

<footer class="text-center">
    <a class="toTop" title="TO TOP"
       href="#" data-toggle="tooltip">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
    <br><br>
    <p>Servizio per la gestione degli esami universitari.</p>
    <p>Rasi - Riciputi - Semprini</p>
</footer>

</body>

</html>
