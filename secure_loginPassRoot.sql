-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:80
-- Creato il: Gen 30, 2017 alle 18:08
-- Versione del server: 5.7.17-0ubuntu0.16.04.1
-- Versione PHP: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `secure_login`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(1, '1485770794'),
(1, '1485773346'),
(2, '1485774054'),
(2, '1485774090'),
(2, '1485774208');

-- --------------------------------------------------------

--
-- Struttura della tabella `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'Luca Semprini', 'lucsemprini@gmail.com', 'd562d92c0f31b928abdd64f32b9b9d2683647f25b05683596a49d3d35251c815d5b4029ec18fc4ecf5e16ecb89b14a2dd336a45e7ad330dda2f253473118738f', 'ba35e170ab646e499bf00ff45da049231524b2891f583a6b0201f30d5727998d309863aedf7ca3d84a3201d8650357497e5ff67900bf4573b21851a7271c2e54'),
(2, 'pinco', 'pinco.pallo@gmail.com', 'd562d92c0f31b928abdd64f32b9b9d2683647f25b05683596a49d3d35251c815d5b4029ec18fc4ecf5e16ecb89b14a2dd336a45e7ad330dda2f253473118738f', 'ba35e170ab646e499bf00ff45da049231524b2891f583a6b0201f30d5727998d309863aedf7ca3d84a3201d8650357497e5ff67900bf4573b21851a7271c2e54'),
(3, 'rasi', 'stefano.rasi@hotmail.it', 'd562d92c0f31b928abdd64f32b9b9d2683647f25b05683596a49d3d35251c815d5b4029ec18fc4ecf5e16ecb89b14a2dd336a45e7ad330dda2f253473118738f', 'ba35e170ab646e499bf00ff45da049231524b2891f583a6b0201f30d5727998d309863aedf7ca3d84a3201d8650357497e5ff67900bf4573b21851a7271c2e54');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
